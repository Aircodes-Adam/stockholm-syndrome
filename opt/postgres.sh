# Install PostgreSQL@13
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql-13 postgresql-contrib

# Performance settings
cd /etc/postgresql/13/main || exit
sudo sed -i 's/max_connections = .*/max_connections = 256/' postgresql.conf
sudo sed -i 's/shared_buffers = */shared_buffers = 512MB/' postgresql.conf
sudo sed -i 's/#temp_buffers = */temp_buffers = 32MB/' postgresql.conf
sudo service postgresql restart

# Backups
[[ -d /var/backups/postgresql ]] || sudo mkdir /var/backups/postgresql
cd /var/backups/postgresql || exit
git init
git checkout -b master
cd || exit
sudo chown -R postgres:postgres /var/backups/postgresql

# Every hour
_command="pg_dump > /var/backups/postgresql/dump_all.sql && git add dump_all.sql && git commit -m backup"
sudo su postgres -c "(crontab -l ; echo \"0 * * * * ${_command}\") | crontab -"

# Every 6 hours
#_command=""
#sudo su postgres -c "(crontab -l ; echo \"0 */6 * * * ${_command}\") | crontab -"

# Every week
#_command=""
#sudo su postgres -c "(crontab -l ; echo \"0 0 */7 * * ${_command}\") | crontab -"
