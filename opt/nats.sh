# Install NATS [Streaming]
_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

curl -L https://github.com/nats-io/nats-server/releases/download/v2.2.1/nats-server-v2.2.1-linux-amd64.zip -o nats-server.zip
unzip nats-server.zip -d nats-server
rm nats-server.zip
sudo mv nats-server/nats-server-v2.2.1-linux-amd64 /opt/nats-server
rm -rf nats-server

# Configure
# https://docs.nats.io/nats-streaming-server/install#installing-a-release-build
sudo adduser --disabled-password --gecos "" nats
sudo wget -O /opt/nats-server/nats.service "${_git_repo}/config/nats.service"
sudo chown -R nats:nats /opt/nats-server
ln -s /opt/nats-server/nats.service /etc/systemd/system/nats.service
sudo systemctl daemon-reload
sudo systemctl enable nats.service
sudo service nats start

