# Install Nginx
_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

sudo apt-get -y install nginx

# Performance settings
sudo wget "${_git_repo}/config/nginx/nginx.conf" -O /etc/nginx/nginx.conf
sudo openssl dhparam -out /etc/nginx/dhparam.pem 2048
sudo mkdir -p /var/www/_letsencrypt
sudo chown www-data /var/www/_letsencrypt
sudo wget "${_git_repo}/config/nginx/cloudflare.conf" -O  /etc/nginx/conf.d/cloudflare.conf

# Importable config files
[[ -d /etc/nginx/conf ]] || sudo mkdir /etc/nginx/conf

sudo wget "${_git_repo}/config/nginx/certbot.conf" -O /etc/nginx/conf/certbot.conf
sudo wget "${_git_repo}/config/nginx/general.conf" -O /etc/nginx/conf/general.conf
sudo wget "${_git_repo}/config/nginx/headers.conf" -O /etc/nginx/conf/headers.conf
sudo wget "${_git_repo}/config/nginx/proxy.conf" -O /etc/nginx/conf/proxy.conf
sudo wget "${_git_repo}/config/nginx/security.conf" -O /etc/nginx/conf/security.conf

sudo ufw allow "Nginx Full"
