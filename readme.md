Prepare Ubuntu

```bash
wget https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master/ubuntu.sh && sudo bash ubuntu.sh
```

Install Services

```bash
wget https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master/services.sh && sudo bash services.sh
```